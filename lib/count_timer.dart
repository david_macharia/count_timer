import 'dart:async';

import 'package:flutter/material.dart';

//count_timer
typedef T CountDownTimerBuilder<T extends CountDownTimerHandler>(
    BuildContext ctx, Duration? duration);

typedef Widget? OnEndBuilder(BuildContext ctx, Duration? duration);
typedef Widget? OnStartBuilder(BuildContext ctx, Duration? duration,
    {TriggerAction? flag});
typedef Widget? OnTickBuilder(BuildContext ctx, Duration? duration);
typedef Widget? OnErrorBuilder(BuildContext ctx, Duration? duration);
typedef Widget? OnTimerPauseBuilder(BuildContext ctx, Duration? duration);
typedef Widget? OnTimerResumeBuilder(BuildContext ctx, Duration? duration);
enum TriggerAction {
  START,
  STOP,
  RESTART,
  PAUSE,
  RESUME,
}

class Trigger {
  StreamController<TriggerAction> triggerStream =
      new StreamController<TriggerAction>.broadcast();
  @deprecated
  //use trigger.addEvent(TriggerAction.START)
  start() {
    triggerStream.add(TriggerAction.START);
  }

  disposeTrigger() {
    triggerStream.close();
  }

  addEvent(TriggerAction triggerAction) {
    triggerStream.add(triggerAction);
  }
}

class WrongConfiguration extends AssertionError {
  String? message;
  WrongConfiguration({this.message}) : super(message);
}

class ControllerBridge {
  StreamController _controller = StreamController.broadcast();
  List<StreamSubscription> subscribers =
      List<StreamSubscription>.empty(growable: true);

  link(Function streamReceiver) {
    subscribers.add(_controller.stream.listen((data) {
      streamReceiver(data);
    }));
  }

  add(dynamic data) {
    _controller.add(data);
  }

  close() {
    // subscribers.forEach((s) => s.pause());
    // subscribers.clear();
  }
  dispose() {
    _controller.close();
  }
}

class CountDownController {
  Duration? end;
  Duration? start;
  Duration? tickingStep;

  ControllerBridge controllerBridge = ControllerBridge();
  CountDownController({this.start, this.end, this.tickingStep}) {
    if (start == null) {
      start = Duration(seconds: 60);
    }
    if (tickingStep == null) {
      tickingStep = Duration(seconds: 1);
    }
  }

  void restart() {
    controllerBridge.add("restart");
  }

  set setStart(Duration? start) => this.start = start;
}

class CountDownTimer extends StatefulWidget {
  CountDownController? countDownController;
  OnEndBuilder? onEndBuilder;
  OnStartBuilder? onStartBuilder;
  OnTickBuilder? onTickBuilder;
  OnTimerPauseBuilder? onTimerPauseBuilder;
  OnTimerResumeBuilder? onTimerResumeBuilder;
  OnErrorBuilder? onErrorBuilder;
  Trigger? triggerStart;
  bool startInstant;
  CountDownTimerBuilder? countDownTimerBuilder;

  CountDownTimer({
    this.onEndBuilder,
    this.onStartBuilder,
    this.onTickBuilder,
    this.countDownController,
    this.onTimerPauseBuilder,
    this.onTimerResumeBuilder,
    this.onErrorBuilder,
    this.triggerStart,
    this.startInstant = true,
    Key? key,
  }) : super(key: key) {
    if (countDownController == null) {
      this.countDownController = CountDownController();
    }
    if (this.triggerStart != null) {
      this.startInstant = false;
    }
    if (this.triggerStart == null && this.startInstant == false) {
      throw new WrongConfiguration(
          message: "trigger cannot be null if instant start is false");
    }
  }
  CountDownTimer.mixing({
    @required this.countDownTimerBuilder,
    this.triggerStart,
    this.startInstant = true,
    this.countDownController,
    Key? key,
  }) : super(key: key) {
    if (countDownController == null) {
      this.countDownController = CountDownController();
    }
    if (this.triggerStart != null) {
      this.startInstant = false;
    }
    if (this.triggerStart == null && this.startInstant == false) {
      throw new WrongConfiguration(
          message: "trigger cannot be null if instant start is false");
    }
  }

  @override
  _CountDownTimerState createState() => _CountDownTimerState();
}

enum DurationEventType { RESTART, RESUME, PAUSE, END, START, ERROR, TICK }

class DurationEvent {
  Duration? duration;
  DurationEventType? durationEventType;
  DurationEvent({this.duration, this.durationEventType});
}

class _CountDownTimerState extends State<CountDownTimer> {
  StreamController<DurationEvent> streamController =
      new StreamController<DurationEvent>.broadcast();
  Timer? timer;
  Duration? lastClock;
  initState() {
    widget.countDownController!.controllerBridge.link((data) {
      if (data == "restart") {
        print("some data");
        partialDispose();
        _build();
      }
    });
    super.initState();
    _build();
  }

  _timerBuild({Duration? duration}) {
    timer = Timer.periodic(this.widget.countDownController!.tickingStep!,
        (Timer t) {
      int dif = this.widget.countDownController!.start!.inSeconds - t.tick;
      if (duration != null) {
        dif = duration.inSeconds - t.tick;
        print("resume diff  $dif");
      }
      if (dif <= 0) {
        streamController.sink.add(DurationEvent(
            duration: Duration(seconds: dif),
            durationEventType: DurationEventType.END));
      } else {
        streamController.sink.add(DurationEvent(
            duration: Duration(seconds: dif),
            durationEventType: DurationEventType.TICK));
      }
    });
    streamController.stream.listen((duration) {
      if (duration.durationEventType == DurationEventType.END) {
        timer?.cancel();
      }
    });
  }

  _build() {
    if (this.widget.startInstant) {
      print("start auto");
      _timerBuild();
    } else {
      this.widget.triggerStart!.triggerStream.stream.listen((data) {
        switch (data) {
          case TriggerAction.START:
            this.lastClock = null;
            if (timer == null || timer?.isActive == false) {
              _timerBuild();
            }

            // TODO: Handle this case.
            break;
          case TriggerAction.STOP:
            this.lastClock = null;
            if (timer != null || timer?.isActive == true) {
              int dif = this.widget.countDownController!.start!.inSeconds -
                  timer!.tick;
              streamController.sink.add(DurationEvent(
                  duration: Duration(seconds: dif),
                  durationEventType: DurationEventType.END));
              timer?.cancel();
              timer = null;
            }
            // TODO: Handle this case.
            break;
          case TriggerAction.RESTART:
            this.lastClock = null;
            if (timer != null || timer?.isActive == true) {
              int dif = this.widget.countDownController!.start!.inSeconds -
                  timer!.tick;

              streamController.sink.add(DurationEvent(
                  duration: Duration(seconds: dif),
                  durationEventType: DurationEventType.RESTART));
              timer?.cancel();
              _timerBuild();
            }
            // streamController.sink.add(DurationEvent(
            //     duration: Duration(seconds: dif),
            //     durationEventType: DurationEventType.END));
            // TODO: Handle this case.
            break;
          case TriggerAction.PAUSE:
            if (this.timer != null && this.timer!.isActive == true) {
              int dif = this.widget.countDownController!.start!.inSeconds -
                  timer!.tick;
              this.widget.countDownController!.setStart =
                  Duration(seconds: dif);
              this.lastClock = Duration(seconds: dif);
              streamController.sink.add(DurationEvent(
                  duration: Duration(seconds: dif),
                  durationEventType: DurationEventType.PAUSE));
            }

            // TODO: Handle this case.
            break;
          case TriggerAction.RESUME:
            if ((timer != null || timer?.isActive == true) &&
                this.lastClock != null) {
              print("${this.lastClock?.inSeconds}");
              int dif = this.widget.countDownController!.start!.inSeconds -
                  timer!.tick;

              streamController.sink.add(DurationEvent(
                  duration: Duration(seconds: dif),
                  durationEventType: DurationEventType.RESUME));
              timer?.cancel();
              timer = null;
              _timerBuild(duration: this.lastClock);
              this.lastClock = null;
            }
            break;
        }

        if (data == "triggered") {
          print("trigger stream");
        } else {}
      });
    }
  }

  partialDispose() {
    widget.countDownController!.controllerBridge.close();

    timer?.cancel();
  }

  void dispose() {
    widget.countDownController!.controllerBridge.dispose();
    streamController.close();
    timer?.cancel();
    widget.triggerStart!.disposeTrigger();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<DurationEvent>(
      stream: streamController.stream,
      builder: (ctx, projectSnap) {
        if (projectSnap.connectionState == ConnectionState.waiting &&
            projectSnap.hasData == false) {
          CountDownTimerHandler countDownTimerBuilder =
              this.widget.countDownTimerBuilder!(context, Duration(seconds: 0));
          countDownTimerBuilder.initialTime =
              this.widget.countDownController!.start;
          if (projectSnap.connectionState == ConnectionState.waiting &&
              projectSnap.hasData == false) {
            if (this.widget.countDownTimerBuilder != null) {
              return countDownTimerBuilder.onStartBuilder(context, null,
                      flag: null) ??
                  countDownTimerBuilder as Widget;
            } else if (this.widget.onStartBuilder != null) {
              return this.widget.onStartBuilder!(context, null, flag: null) ??
                  Container();
            }
          }

          return Container();
        } else {
          if (projectSnap.hasData) {
            Duration? duration = projectSnap.data!.duration;
            if (this.widget.countDownTimerBuilder == null) {
              switch (projectSnap.data!.durationEventType) {
                case DurationEventType.END:
                  if (this.widget.onEndBuilder != null) {
                    return this.widget.onEndBuilder!(context, duration) ??
                        Container();
                  }
                  return SizedBox();
                  // TODO: Handle this case.
                  break;
                case DurationEventType.START:
                  if (this.widget.onStartBuilder != null) {
                    return this.widget.onStartBuilder!(context, duration) ??
                        Container();
                  }
                  return SizedBox();
                  // TODO: Handle this case.
                  break;
                case DurationEventType.ERROR:
                  if (this.widget.onErrorBuilder != null) {
                    return this.widget.onEndBuilder!(context, duration) ??
                        Container();
                  }
                  return SizedBox();
                  // TODO: Handle this case.
                  break;
                case DurationEventType.TICK:
                  if (this.widget.onTickBuilder != null) {
                    return this.widget.onTickBuilder!(context, duration) ??
                        Container();
                  }
                  break;

                case DurationEventType.RESTART:
                  if (this.widget.onStartBuilder != null) {
                    return this.widget.onStartBuilder!(context, duration,
                            flag: TriggerAction.RESTART) ??
                        Container();
                  }
                  break;
                case DurationEventType.RESUME:
                  if (this.widget.onTimerResumeBuilder != null) {
                    return this.widget.onTimerResumeBuilder!(
                            context, duration) ??
                        Container();
                  }
                  break;
                case DurationEventType.PAUSE:
                  if (this.widget.onTimerPauseBuilder != null) {
                    timer?.cancel();
                    return this.widget.onTimerPauseBuilder!(
                            context, duration) ??
                        Container();
                  }
                  break;

                default:
                  return SizedBox();
              }
            } else {
              CountDownTimerHandler countDownTimerBuilder =
                  this.widget.countDownTimerBuilder!(context, duration);
                      countDownTimerBuilder.initialTime =
              this.widget.countDownController!.start;
              switch (projectSnap.data!.durationEventType) {
                case DurationEventType.END:
                  return countDownTimerBuilder.onEndBuilder(ctx, duration) ??
                      countDownTimerBuilder as Widget;

                  // TODO: Handle this case.
                  break;
                case DurationEventType.START:
                  return countDownTimerBuilder.onStartBuilder(
                          context, duration) ??
                      countDownTimerBuilder as Widget;

                  // TODO: Handle this case.
                  break;
                case DurationEventType.ERROR:
                  return countDownTimerBuilder.onEndBuilder(
                          context, duration) ??
                      countDownTimerBuilder as Widget;
                  ;

                  // TODO: Handle this case.
                  break;
                case DurationEventType.TICK:
                  return countDownTimerBuilder.onTickBuilder(
                          context, duration) ??
                      countDownTimerBuilder as Widget;

                  break;

                case DurationEventType.RESTART:
                  return countDownTimerBuilder.onStartBuilder(context, duration,
                          flag: TriggerAction.RESTART) ??
                      countDownTimerBuilder as Widget;
                  ;

                  break;
                case DurationEventType.RESUME:
                  return countDownTimerBuilder.onTimerResumeBuilder(
                          context, duration) ??
                      countDownTimerBuilder as Widget;

                case DurationEventType.PAUSE:
                  timer?.cancel();
                  return countDownTimerBuilder.onTimerPauseBuilder(
                          context, duration) ??
                      countDownTimerBuilder as Widget;
                  ;

                default:
                  return SizedBox();
              }
            }
          }
        }

        return SizedBox();
      },
    );
  }
}

abstract class CountDownTimerHandler {
  Duration? initialTime;
  Widget? onEndBuilder(BuildContext ctx, Duration? duration);
  Widget? onStartBuilder(BuildContext ctx, Duration? duration,
      {TriggerAction? flag});
  Widget? onTickBuilder(BuildContext ctx, Duration? duration);
  Widget? onErrorBuilder(BuildContext ctx, Duration? duration);
  Widget? onTimerPauseBuilder(BuildContext ctx, Duration? duration);
  Widget? onTimerResumeBuilder(BuildContext ctx, Duration? duration);
}
