# count_timer

Simple Elegant seconds count down Well defined interface

## Getting Started
### fancy demo
![Demo Image](https://bitbucket.org/david_macharia/count_timer/src/master/sample/scree4.png)
##### import
```dart
import 'package:count_timer/count_timer.dart';
```
### initialise trigger
```dart
 CountDownController countDownController =
      CountDownController(start: Duration(seconds: 20));
  Trigger trigger = Trigger();
```
 ### Example
``` dart
Expanded(
                    child: Container(
                      child: Center(
                          child: CountDownTimer(
                        startInstant: true,
                        triggerStart: trigger,
                        onTimerResumeBuilder: (ctx, duration) {
                          return Text("resume");
                        },
                        countDownController: countDownController,
                        onTimerPauseBuilder: (ctx, duration) {
                          print("difference is ${duration!.inSeconds}");
                          return Text(
                              "Counter Paused at ${duration.inSeconds}");
                        },
                        onStartBuilder: (ctx, duration, {flag}) {
                          return StareCase();
                          //  if (flag == TriggerAction.RESTART) {
                          //    return Text("restarting");
                          //  }
                          return Text("Counter Start Builder");
                        },
                        onTickBuilder: (ctx, duration) {
                          return Text("ticking builder ${duration?.inSeconds}");
                        },
                        onEndBuilder: (ctx, duration) {
                          return Text("My counter end Builder");
                        },
                      )),
                    ),
                  ) 
                  
   
              
```
You can event use a mixin to make your state listen to count_down timer
``` dart
Expanded(
    child: Container(
        child: Center(
          child: CountDownTimer.mixing(
            startInstant: true,
            triggerStart: trigger,
            countDownController: this.countDownController,
            countDownTimerBuilder:(BuildContext ctx, Duration? duration) {
                 return StareCase();
                },
              )),
             ),
         ),
```
Controlling count down timer

``` dart
  Row( mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            TextButton(
                              child: Text("START"),
                              onPressed: () {
                                trigger.addEvent(TriggerAction.START);
                                // trigger.start();
                              },
                            ),
                            TextButton(
                              child: Text("STOP"),
                              onPressed: () {
                                trigger.addEvent(TriggerAction.STOP);
                              },
                            ),
                            TextButton(
                              child: Text("RESTART"),
                              onPressed: () {
                                // countDownController.restart();
                                trigger.addEvent(TriggerAction.RESTART);
                              },
                            ),
                            TextButton(
                              child: Text("PAUSE"),
                              onPressed: () {
                                trigger.addEvent(TriggerAction.PAUSE);
                              },
                            ),
                            TextButton(
                              child: Text("RESUME"),
                              onPressed: () {
                                trigger.addEvent(TriggerAction.RESUME);
                              },
                            ),
                          ],
                        ),
                      
                      
```
