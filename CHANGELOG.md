# Change Log for changelog
Helps to write your CHANGELOG.md

## [1.2.0](https://david_macharia@bitbucket.org/david_macharia/count_timer.git) - 2020-4-19

### Feature
* Console-output comes without date + Logger-name [9f77003](https://david_macharia@bitbucket.org/david_macharia/count_timer.git)

