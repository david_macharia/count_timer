import 'dart:math';

import 'package:count_timer/count_timer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/physics.dart';

void main() {
  runApp(ImageBuilder());
}

class ImageBuilder extends StatelessWidget {
  ImageBuilder({Key? key}) : super(key: key);
  CountDownController countDownController =
      CountDownController(start: Duration(seconds: 20));
  Trigger trigger = Trigger();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(
            textTheme: TextTheme(headline2: TextStyle(color: Colors.white)),
            primaryColor: Colors.yellow),
        home: Container(
            child: Scaffold(
          body: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    fit: BoxFit.cover,
                    image: AssetImage("assets/images/background.jpg"))),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Container(
                      child: Center(
                          child: CountDownTimer(
                        startInstant: true,
                        triggerStart: trigger,
                        onTimerResumeBuilder: (ctx, duration) {
                          return Text("resume");
                        },
                        countDownController: countDownController,
                        onTimerPauseBuilder: (ctx, duration) {
                          print("difference is ${duration!.inSeconds}");
                          return Text(
                              "Counter Paused at ${duration.inSeconds}");
                        },
                        onStartBuilder: (ctx, duration, {flag}) {
                          return StareCase();
                          //  if (flag == TriggerAction.RESTART) {
                          //    return Text("restarting");
                          //  }
                          return Text("Counter Start Builder");
                        },
                        onTickBuilder: (ctx, duration) {
                          return Text("ticking builder ${duration?.inSeconds}");
                        },
                        onEndBuilder: (ctx, duration) {
                          return Text("My counter end Builder");
                        },
                      )),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      child: Center(
                          child: CountDownTimer.mixing(
                        startInstant: true,
                        triggerStart: trigger,
                        countDownController: this.countDownController,
                        countDownTimerBuilder:
                            (BuildContext ctx, Duration? duration) {
                          return StareCase();
                        },
                      )),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
                    decoration:
                        BoxDecoration(border: Border.all(color: Colors.grey)),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          "Control Panel",
                          style: TextStyle(color: Colors.white),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            TextButton(
                              child: Text("START"),
                              onPressed: () {
                                trigger.addEvent(TriggerAction.START);
                                // trigger.start();
                              },
                            ),
                            TextButton(
                              child: Text("STOP"),
                              onPressed: () {
                                trigger.addEvent(TriggerAction.STOP);
                              },
                            ),
                            TextButton(
                              child: Text("RESTART"),
                              onPressed: () {
                                // countDownController.restart();
                                trigger.addEvent(TriggerAction.RESTART);
                              },
                            ),
                            TextButton(
                              child: Text("PAUSE"),
                              onPressed: () {
                                trigger.addEvent(TriggerAction.PAUSE);
                              },
                            ),
                            TextButton(
                              child: Text("RESUME"),
                              onPressed: () {
                                trigger.addEvent(TriggerAction.RESUME);
                              },
                            ),
                          ],
                        ),
                      
                      
                      ],
                    ),
                  )
                ]),
          ),
        )));
  }
}

class StareCase extends StatelessWidget with CountDownTimerHandler {
  Color stare1 = Colors.yellow;
  double stare1HeightRatio = 0.2;
  static double count = -1;
  Duration? duration = Duration.zero;

  buildRocket() {
    return Transform.rotate(
        angle: -0.8,
        child: Text(
          " 🚀",
          style: TextStyle(fontSize: 40),
        ));
  }

  buildStarShip() {
    return Image.asset("assets/images/starship.png");
  }

  buildFalconHeavy() {
    return Image.asset("assets/images/falcon_heavy.png");
  }

  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return SafeArea(
      child: Stack(
        children: [
          Container(
            alignment: Alignment.bottomLeft,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Container(
                  height: height * 0.15,
                  width: width * 0.15,
                  child: Stack(
                    alignment: Alignment.bottomCenter,
                    children: [
                      AnimatedAlign(
                          alignment: Alignment(0, count),
                          duration: Duration(seconds: 1),
                          child: Ball())
                    ],
                  ),
                  decoration: BoxDecoration(color: stare1),
                ),
                Container(
                  height: height * 0.35,
                  width: width * 0.15,
                  child: Stack(
                    children: [
                      AnimatedAlign(
                          alignment: Alignment(0, count),
                          duration: Duration(seconds: 1),
                          child: buildRocket())
                    ],
                  ),
                  decoration: BoxDecoration(color: Colors.yellow),
                ),
                Container(
                  height: height * 0.45,
                  width: width * 0.15,
                  child: Stack(
                    children: [
                      AnimatedAlign(
                          alignment: Alignment(0, count),
                          duration: Duration(seconds: 1),
                          child: buildFalconHeavy())
                    ],
                  ),
                  decoration: BoxDecoration(color: Colors.yellow),
                ),
                Container(
                  height: height * 0.7,
                  width: width * 0.15,
                  child: Stack(
                    children: [
                      AnimatedAlign(
                          alignment: Alignment(0, count),
                          duration: Duration(seconds: 1),
                          child: buildStarShip())
                    ],
                  ),
                  decoration: BoxDecoration(color: Colors.yellow),
                ),
              ],
            ),
          ),
          Align(
              alignment: Alignment.topCenter,
              child: Text("${duration?.inSeconds}"))
        ],
      ),
    );
  }

  @override
  Widget? onEndBuilder(BuildContext ctx, Duration? duration) {
    count = 0;
    return this;
  }

  @override
  Widget? onErrorBuilder(BuildContext ctx, Duration? duration) {}

  @override
  Widget? onStartBuilder(BuildContext ctx, Duration? duration,
      {TriggerAction? flag}) {
    print("called");
  }

  @override
  Widget? onTickBuilder(BuildContext ctx, Duration? duration) {
    //double h1 = MediaQuery.of(ctx).size.height * 0.15;
    this.stare1HeightRatio = 0;
    count = -(-duration!.inSeconds.toDouble() + 20);
    this.duration = duration;
    print("count ${count}");
  }

  @override
  Widget? onTimerPauseBuilder(BuildContext ctx, Duration? duration) {}

  @override
  Widget? onTimerResumeBuilder(BuildContext ctx, Duration? duration) {}
}

class Ball extends StatefulWidget {
  @override
  _BallState createState() => _BallState();
}

class _BallState extends State<Ball> with SingleTickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    return buildBall();
  }

  buildBall() {
    return Image.asset("assets/images/ball.png");
  }
}
